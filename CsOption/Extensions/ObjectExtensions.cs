﻿
namespace Options.Extensions
{
    /// <summary>
    /// Extensions für generelles arbeiten mit Objekten. 
    /// </summary>
    public static class ObjectExtensions
    {
        /// <summary>
        /// Extension zum umwandeln von Nullable Typen zu <see cref="Option{T}"/>. Wenn null auftritt wird null zu None.
        /// </summary>
        /// <typeparam name="T">Jeder beliebige Referenztyp.</typeparam>
        /// <param name="value">Wert der zum <see cref="Option"/> gemacht werden soll.</param>
        /// <returns>Gibt ein Option mit Some zurück wenn <paramref name="value"/> nicht null ist ansonsten ein None.</returns>
        public static Option<T> NoneIfNull<T>(this T value) where T : class
            => value is null ? None.Value : (Option<T>)new Some<T>(value);

        /// <summary>
        /// Checks if an Option value is <see cref="Some{T}"/>.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="value"></param>
        /// <returns></returns>
        public static bool IsSome<T>(this Option<T> value)
            => value is Some<T>;

        /// <summary>
        /// Checks if an Option value is <see cref="None{T}"/>.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="value"></param>
        /// <returns></returns>
        public static bool IsNone<T>(this Option<T> value)
            => value is None<T>;
    }
}
