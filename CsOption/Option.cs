﻿
namespace Options
{
    /// <summary>
    /// Basis für den Option Typ in C#. Überlädt den impliziten Konvertierungsoperator um das schreiben von Code zu vereinfachen.
    /// Sollte mit jedem beliebigen Typ funktionieren
    /// </summary>
    /// <typeparam name="T">Jeder beliebige Typ.</typeparam>
    public abstract class Option<T>
    {
        /// <summary>
        /// Impliziertes Konvertieren von <see cref="Option{T}"/> zu <see cref="Some{T}"/>. 
        /// </summary>
        public static implicit operator Option<T>(T value) =>
            new Some<T>(value);

        /// <summary>
        /// Impliziertes Konvertieren von <see cref="Option{T}"/> zu <see cref="None{T}"/>.
        /// </summary>
        public static implicit operator Option<T>(None _) =>
            new None<T>();
    }
}
